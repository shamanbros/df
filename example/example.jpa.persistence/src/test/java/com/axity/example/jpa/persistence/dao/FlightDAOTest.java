package com.axity.example.jpa.persistence.dao;

import static org.junit.Assert.fail;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.axity.example.jpa.model.FlightDO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/initial-test-context.xml" })
@Transactional
public class FlightDAOTest
{
  @Autowired
  private FlightDAO flightDAO;

  @Test
  public void testFindByDates()
  {
    Calendar c1 = Calendar.getInstance();
    c1.set( 2018, Calendar.APRIL, 1, 0, 0, 0 );
    c1.set( Calendar.MILLISECOND, 0 );
    Date start = c1.getTime();

    Calendar c2 = Calendar.getInstance();
    c2.set( 2018, Calendar.APRIL, 1, 23, 59, 59 );
    c2.set( Calendar.MILLISECOND, 999 );
    Date end = c2.getTime();

    List<FlightDO> flights = this.flightDAO.findByDates( start, end );
    Assert.assertNotNull( flights );
    Assert.assertEquals( 147, flights.size() );
  }

  @Test
  public void testFindByRouteCode()
  {

    String code = "BOGMIA";
    Calendar c1 = Calendar.getInstance();
    c1.set( 2018, Calendar.APRIL, 1, 0, 0, 0 );
    c1.set( Calendar.MILLISECOND, 0 );
    Date start = c1.getTime();

    Calendar c2 = Calendar.getInstance();
    c2.set( 2018, Calendar.APRIL, 1, 23, 59, 59 );
    c2.set( Calendar.MILLISECOND, 999 );
    Date end = c2.getTime();

    List<FlightDO> flights = this.flightDAO.findByRouteCode( code, start, end );
    Assert.assertNotNull( flights );
    Assert.assertEquals( 2, flights.size() );
  }

  @Test
  public void testFindByStationDepartureCode()
  {
    String departureStationCode = "BOG";
    Calendar c1 = Calendar.getInstance();
    c1.set( 2018, Calendar.APRIL, 1, 0, 0, 0 );
    c1.set( Calendar.MILLISECOND, 0 );
    Date start = c1.getTime();

    Calendar c2 = Calendar.getInstance();
    c2.set( 2018, Calendar.APRIL, 1, 23, 59, 59 );
    c2.set( Calendar.MILLISECOND, 999 );
    Date end = c2.getTime();

    List<FlightDO> flights = this.flightDAO.findByStationDepartureCode( departureStationCode, start, end );
    Assert.assertNotNull( flights );
    Assert.assertEquals( 73, flights.size() );
  }

  @Test
  public void testFindByCarrier()
  {
    String carrierCode = "AV";
    Calendar c1 = Calendar.getInstance();
    c1.set( 2018, Calendar.APRIL, 1, 0, 0, 0 );
    c1.set( Calendar.MILLISECOND, 0 );
    Date start = c1.getTime();

    Calendar c2 = Calendar.getInstance();
    c2.set( 2018, Calendar.APRIL, 1, 23, 59, 59 );
    c2.set( Calendar.MILLISECOND, 999 );
    Date end = c2.getTime();

    List<FlightDO> flights = this.flightDAO.findByCarrier( carrierCode, start, end );
    Assert.assertNotNull( flights );
    Assert.assertEquals( 117, flights.size() );
  }

  @Test
  public void testFindByRegistration()
  {
    String registration = "HK4954";
    Calendar c1 = Calendar.getInstance();
    c1.set( 2018, Calendar.APRIL, 1, 0, 0, 0 );
    c1.set( Calendar.MILLISECOND, 0 );
    Date start = c1.getTime();

    Calendar c2 = Calendar.getInstance();
    c2.set( 2018, Calendar.APRIL, 1, 23, 59, 59 );
    c2.set( Calendar.MILLISECOND, 999 );
    Date end = c2.getTime();

    List<FlightDO> flights = this.flightDAO.findByRegistration( registration, start, end );
    Assert.assertNotNull( flights );
    Assert.assertEquals( 2, flights.size() );
  }

  @Test
  public void testFindByFlight()
  {
    Integer flight = 9278;
    Calendar c1 = Calendar.getInstance();
    c1.set( 2018, Calendar.APRIL, 3, 0, 0, 0 );
    c1.set( Calendar.MILLISECOND, 0 );
    Date start = c1.getTime();

    Calendar c2 = Calendar.getInstance();
    c2.set( 2018, Calendar.APRIL, 4, 23, 59, 59 );
    c2.set( Calendar.MILLISECOND, 999 );
    Date end = c2.getTime();

    List<FlightDO> flights = this.flightDAO.findByFlight( flight, start, end );
    Assert.assertNotNull( flights );
    Assert.assertEquals( 2, flights.size() );
  }

  @Test
  public void testFindByDelayDeparture()
  {
    Calendar c1 = Calendar.getInstance();
    c1.set( 2018, Calendar.APRIL, 1, 0, 0, 0 );
    c1.set( Calendar.MILLISECOND, 0 );
    Date start = c1.getTime();

    Calendar c2 = Calendar.getInstance();
    c2.set( 2018, Calendar.APRIL, 1, 23, 59, 59 );
    c2.set( Calendar.MILLISECOND, 999 );
    Date end = c2.getTime();

    List<FlightDO> flights = this.flightDAO.findByDelayDeparture( start, end );
    Assert.assertNotNull( flights );
    Assert.assertEquals( 80, flights.size() );
  }

  @Test
  public void testFindByDelayArrive()
  {
    Calendar c1 = Calendar.getInstance();
    c1.set( 2018, Calendar.APRIL, 1, 0, 0, 0 );
    c1.set( Calendar.MILLISECOND, 0 );
    Date start = c1.getTime();

    Calendar c2 = Calendar.getInstance();
    c2.set( 2018, Calendar.APRIL, 1, 23, 59, 59 );
    c2.set( Calendar.MILLISECOND, 999 );
    Date end = c2.getTime();

    List<FlightDO> flights = this.flightDAO.findByDelayArrive( start, end );
    Assert.assertNotNull( flights );
    Assert.assertEquals( 147, flights.size() );
  }

}
