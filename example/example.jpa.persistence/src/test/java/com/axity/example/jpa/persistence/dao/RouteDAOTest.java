package com.axity.example.jpa.persistence.dao;

import java.text.DecimalFormat;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.axity.example.jpa.model.RouteDO;
import com.axity.example.jpa.persistence.to.FlightTO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/initial-test-context.xml" })
@Transactional
public class RouteDAOTest
{

  @Autowired
  private RouteDAO routeDAO;

  @Test
  public void testFindRoutesByAircraftRegistration()
  {

    String registration = "HK4954";
    List<RouteDO> routes = routeDAO.findRoutesByAircraftRegistration( registration );

    System.out.println( new DecimalFormat( "0000" ).format( 3 ) );
    Assert.assertNotNull( routes );

    for( RouteDO route : routes )
    {
      System.out.println( route );
    }
  }
  
  @Test
  public void testFindFlightsByAircraftRegistration()
  {

    String registration = "HK4954";
    List<FlightTO> flights = routeDAO.findFlightsByAircraftRegistration( registration );

    Assert.assertNotNull( flights );

    for( FlightTO flight : flights )
    {
      System.out.println( flight );
    }
  }

}
