package com.axity.example.jpa.persistence.dao;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/initial-test-context.xml" })
@Transactional
public class QueryAPITest
{

  @PersistenceContext(unitName = "ExamplePersistenceUnit")
  private EntityManager entityManager;

  @Test
  public void testCreateQuery()
  {
    Query query = entityManager.createQuery( "SELECT o FROM CarrierDO o" );

    List result = query.getResultList();
    Assert.assertNotNull( result );
    for( Object o : result )
    {
      System.out.println( o );
    }

  }

  @Test
  public void testCreateQueryWithParams()
  {
    Query query = entityManager.createQuery( "SELECT o FROM CarrierDO o WHERE o.code = :code" );
    query.setParameter( "code", "AV" );
    List result = query.getResultList();
    Assert.assertNotNull( result );
    for( Object o : result )
    {
      System.out.println( o );
    }

  }

  @Test
  public void testCreateQueryDate() throws ParseException
  {
    Query query = entityManager
        .createQuery( "SELECT o FROM FlightDO o WHERE o.scheduledDepartureUtc BETWEEN :start AND :end" );

    Calendar c1 = Calendar.getInstance();
    c1.set( 2018, Calendar.APRIL, 1, 0, 0, 0 );
    c1.set( Calendar.MILLISECOND, 0 );
    Date start = c1.getTime();

    Calendar c2 = Calendar.getInstance();
    c2.set( 2018, Calendar.APRIL, 1, 23, 59, 59 );
    c2.set( Calendar.MILLISECOND, 999 );
    Date end = c2.getTime();
    
    
    DateFormat df = new SimpleDateFormat( "yyyy-MM-dd HH:mm" );
    start = df.parse( "2018-04-01 00:00:00" );
    end = df.parse( "2018-04-01 23:59:99" );
    
    query.setParameter( "start", start );
    query.setParameter( "end", end );
    
    
    List result = query.getResultList();
    Assert.assertNotNull( result );
    for( Object o : result )
    {
      System.out.println( o );
    }

  }

}
