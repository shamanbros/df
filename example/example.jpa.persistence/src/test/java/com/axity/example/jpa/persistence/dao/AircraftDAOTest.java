package com.axity.example.jpa.persistence.dao;

import static org.junit.Assert.fail;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.axity.example.jpa.model.AircraftDO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/initial-test-context.xml" })
@Transactional
public class AircraftDAOTest
{

  @Autowired
  private AircraftDAO aircraftDAO;

  @Test
  public void testFindByRegistration()
  {
    String registration = "HK4954";
    List<AircraftDO> aircrafts = this.aircraftDAO.findByRegistration( registration );
    Assert.assertNotNull( aircrafts );
    Assert.assertEquals( 1, aircrafts.size() );
  }

  @Test
  public void testFindByCarrierId()
  {
    Integer idCarrier = 1;
    List<AircraftDO> aircrafts = this.aircraftDAO.findByCarrierId( idCarrier );
    Assert.assertNotNull( aircrafts );
    Assert.assertEquals( 67, aircrafts.size() );
  }

  @Test
  public void testFindByCarrierCode()
  {
    String code = "AV";
    List<AircraftDO> aircrafts = this.aircraftDAO.findByCarrierCode( code );
    Assert.assertNotNull( aircrafts );
    Assert.assertEquals( 67, aircrafts.size() );
  }

  @Test
  public void testFindByAircraftId()
  {
    Integer idAircraft = 8;
    List<AircraftDO> aircrafts = this.aircraftDAO.findByAircraftId( idAircraft );
    Assert.assertNotNull( aircrafts );
    Assert.assertEquals( 5, aircrafts.size() );
  }

  @Test
  public void testFindByAircraftCode()
  {
    String code = "321";
    List<AircraftDO> aircrafts = this.aircraftDAO.findByAircraftCode( code );
    Assert.assertNotNull( aircrafts );
    Assert.assertEquals( 5, aircrafts.size() );
  }

  @Test
  public void testFindByRouteCode()
  {
    String code = "BOGMIA";
    Calendar c1 = Calendar.getInstance();
    c1.set( 2018, Calendar.APRIL, 1, 0, 0, 0 );
    c1.set( Calendar.MILLISECOND, 0 );
    Date start = c1.getTime();

    Calendar c2 = Calendar.getInstance();
    c2.set( 2018, Calendar.APRIL, 1, 23, 59, 59 );
    c2.set( Calendar.MILLISECOND, 999 );
    Date end = c2.getTime();
    
    List<AircraftDO> aircrafts = this.aircraftDAO.findByRouteCode(code, start, end);
    Assert.assertNotNull( aircrafts );
    Assert.assertEquals( 2, aircrafts.size() );
  }

}
