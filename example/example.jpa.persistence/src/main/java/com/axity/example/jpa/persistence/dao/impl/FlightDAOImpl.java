package com.axity.example.jpa.persistence.dao.impl;

import java.util.Date;
import java.util.List;

import javax.persistence.TypedQuery;

import com.axity.example.jpa.model.FlightDO;
import com.axity.example.jpa.persistence.base.GenericBaseDAOImpl;
import com.axity.example.jpa.persistence.dao.FlightDAO;

public class FlightDAOImpl extends GenericBaseDAOImpl<FlightDO> implements FlightDAO
{

  public FlightDAOImpl()
  {
    super( FlightDO.class );
  }

  @Override
  public List<FlightDO> findByDates( Date start, Date end )
  {
    TypedQuery<FlightDO> query = getEntityManager().createNamedQuery( "FlightDO.findByDates", FlightDO.class );
    query.setParameter( "start", start );
    query.setParameter( "end", end );
    return query.getResultList();
  }

  @Override
  public List<FlightDO> findByRouteCode( String code, Date start, Date end )
  {
    TypedQuery<FlightDO> query = getEntityManager().createNamedQuery( "FlightDO.findByRouteCode", FlightDO.class );
    query.setParameter( "code", code );
    query.setParameter( "start", start );
    query.setParameter( "end", end );
    return query.getResultList();
  }

  @Override
  public List<FlightDO> findByStationDepartureCode( String departureStationCode, Date start, Date end )
  {
    TypedQuery<FlightDO> query = getEntityManager().createNamedQuery( "FlightDO.findByStationDepartureCode",
      FlightDO.class );
    query.setParameter( "departureStationCode", departureStationCode );
    query.setParameter( "start", start );
    query.setParameter( "end", end );
    return query.getResultList();
  }

  @Override
  public List<FlightDO> findByCarrier( String carrierCode, Date start, Date end )
  {
    TypedQuery<FlightDO> query = getEntityManager().createNamedQuery( "FlightDO.findByCarrier", FlightDO.class );
    query.setParameter( "carrierCode", carrierCode );
    query.setParameter( "start", start );
    query.setParameter( "end", end );
    return query.getResultList();
  }

  @Override
  public List<FlightDO> findByRegistration( String registration, Date start, Date end )
  {
    TypedQuery<FlightDO> query = getEntityManager().createNamedQuery( "FlightDO.findByRegistration", FlightDO.class );
    query.setParameter( "registration", registration );
    query.setParameter( "start", start );
    query.setParameter( "end", end );
    return query.getResultList();
  }

  @Override
  public List<FlightDO> findByFlight( Integer flight, Date start, Date end )
  {
    TypedQuery<FlightDO> query = getEntityManager().createNamedQuery( "FlightDO.findByFlight", FlightDO.class );
    query.setParameter( "flight", flight );
    query.setParameter( "start", start );
    query.setParameter( "end", end );
    return query.getResultList();
  }

  @Override
  public List<FlightDO> findByDelayDeparture( Date start, Date end )
  {
    TypedQuery<FlightDO> query = getEntityManager().createNamedQuery( "FlightDO.findByDelayDeparture", FlightDO.class );
    query.setParameter( "start", start );
    query.setParameter( "end", end );
    return query.getResultList();
  }

  @Override
  public List<FlightDO> findByDelayArrive( Date start, Date end )
  {
    TypedQuery<FlightDO> query = getEntityManager().createNamedQuery( "FlightDO.findByDelayArrive", FlightDO.class );
    query.setParameter( "start", start );
    query.setParameter( "end", end );
    return query.getResultList();
  }

}
