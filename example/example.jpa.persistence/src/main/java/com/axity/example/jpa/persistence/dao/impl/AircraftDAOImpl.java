package com.axity.example.jpa.persistence.dao.impl;

import java.util.Date;
import java.util.List;

import javax.persistence.TypedQuery;

import com.axity.example.jpa.model.AircraftDO;
import com.axity.example.jpa.persistence.base.GenericBaseDAOImpl;
import com.axity.example.jpa.persistence.dao.AircraftDAO;

public class AircraftDAOImpl extends GenericBaseDAOImpl<AircraftDO> implements AircraftDAO
{

  public AircraftDAOImpl()
  {
    super( AircraftDO.class );
  }

  @Override
  public List<AircraftDO> findByRegistration( String registration )
  {
    TypedQuery<AircraftDO> query = getEntityManager().createNamedQuery( "AircraftDO.findByRegistration",
      AircraftDO.class );
    query.setParameter( "registration", registration );
    return query.getResultList();
  }

  @Override
  public List<AircraftDO> findByCarrierId( Integer idCarrier )
  {
    TypedQuery<AircraftDO> query = getEntityManager().createNamedQuery( "AircraftDO.findByCarrierId",
      AircraftDO.class );
    query.setParameter( "idCarrier", idCarrier );
    return query.getResultList();
  }

  @Override
  public List<AircraftDO> findByCarrierCode( String code )
  {
    TypedQuery<AircraftDO> query = getEntityManager().createNamedQuery( "AircraftDO.findByCarrierCode",
      AircraftDO.class );
    query.setParameter( "code", code );
    return query.getResultList();
  }

  @Override
  public List<AircraftDO> findByAircraftId( Integer idAircraft )
  {
    TypedQuery<AircraftDO> query = getEntityManager().createNamedQuery( "AircraftDO.findByAircraftId",
      AircraftDO.class );
    query.setParameter( "idAircraft", idAircraft );
    return query.getResultList();
  }

  @Override
  public List<AircraftDO> findByAircraftCode( String code )
  {
    TypedQuery<AircraftDO> query = getEntityManager().createNamedQuery( "AircraftDO.findByAircraftCode",
      AircraftDO.class );
    query.setParameter( "code", code );
    return query.getResultList();
  }

  @Override
  public List<AircraftDO> findByRouteCode( String code, Date start, Date end )
  {
    TypedQuery<AircraftDO> query = getEntityManager().createNamedQuery( "AircraftDO.findByRouteCode",
      AircraftDO.class );
    query.setParameter( "code", code );
    query.setParameter( "start", start );
    query.setParameter( "end", end );
    return query.getResultList();
  }

}
