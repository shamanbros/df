package com.axity.example.jpa.persistence.dao;

import java.util.Date;
import java.util.List;

import com.axity.example.jpa.model.FlightDO;
import com.axity.example.jpa.persistence.base.GenericDAO;

/**
 * DAO de la tabla C_PAIS
 * 
 * @author gsegura
 */
public interface FlightDAO extends GenericDAO<FlightDO>
{

  /**
   * Busca los vuelos en un rango de tiempo
   * 
   * @param start Fecha de inicio
   * @param end Fecha de fin
   * @return Una lista de {@link com.axity.example.jpa.model.FlightDO}
   */
  List<FlightDO> findByDates( Date start, Date end );

  /**
   * Busca las rutas en un rango de tiempo, por c�digo de ruta i.e. BOGMIA
   * 
   * @param code C�digo de la ruta (salida y llegada), i.e. BOGMIA
   * @param start Fecha de inicio
   * @param end Fecha de fin
   * @return Una lista de {@link com.axity.example.jpa.model.FlightDO}
   */
  List<FlightDO> findByRouteCode( String code, Date start, Date end );

  /**
   * Busca los vuelos en un rango de tiempo, por el codigo de la estacion de salida
   * 
   * @param departureStationCode C�digo de la estaci�n de salida
   * @param start Fecha de inicio
   * @param end Fecha de fin
   * @return Una lista de {@link com.axity.example.jpa.model.FlightDO}
   */
  List<FlightDO> findByStationDepartureCode( String departureStationCode, Date start, Date end );

  /**
   * Busca los vuelos en un rango de tiempo, por el codigo de la aerolinea
   * 
   * @param carrierCode C�digo de la aerol�nea
   * @param start Fecha de inicio
   * @param end Fecha de fin
   * @return Una lista de {@link com.axity.example.jpa.model.FlightDO}
   */
  List<FlightDO> findByCarrier( String carrierCode, Date start, Date end );

  /**
   * Busca los vuelos en un rango de tiempo por la matricula del avion
   * 
   * @param registration Matr�cula de la aeronave
   * @param start Fecha de inicio
   * @param end Fecha de fin
   * @return Una lista de {@link com.axity.example.jpa.model.FlightDO}
   */
  List<FlightDO> findByRegistration( String registration, Date start, Date end );

  /**
   * Busca los vuelos en un rango de tiempo por el numero de vuelo
   * 
   * @param flight N�mero de vuelo
   * @param start Fecha de inicio
   * @param end Fecha de fin
   * @return Una lista de {@link com.axity.example.jpa.model.FlightDO}
   */
  List<FlightDO> findByFlight( Integer flight, Date start, Date end );

  /**
   * Busca los vuelos con demora en salida, mayor a cero, en un rango de tiempo
   * 
   * @param start Fecha de inicio
   * @param end Fecha de fin
   * @return Una lista de {@link com.axity.example.jpa.model.FlightDO}
   */
  List<FlightDO> findByDelayDeparture( Date start, Date end );

  /**
   * Busca los vuelos con demora en llegada, mayor a cero, en un rango de tiempo
   * 
   * @param start Fecha de inicio
   * @param end Fecha de fin
   * @return Una lista de {@link com.axity.example.jpa.model.FlightDO}
   */
  List<FlightDO> findByDelayArrive( Date start, Date end );

}
