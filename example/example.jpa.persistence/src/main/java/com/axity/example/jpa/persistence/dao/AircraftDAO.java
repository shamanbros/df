package com.axity.example.jpa.persistence.dao;

import java.util.Date;
import java.util.List;

import com.axity.example.jpa.model.AircraftDO;
import com.axity.example.jpa.persistence.base.GenericDAO;

/**
 * DAO de la tabla K_AERONAVE
 * 
 * @author gsegura
 */
public interface AircraftDAO extends GenericDAO<AircraftDO>
{

  /**
   * Busca las aeronaves por su numero de registro (matr�cula)
   * 
   * @param registration Matr�cula del avi�n
   * @return una lista de {@link com.axity.example.jpa.model.AircraftDO}
   */
  List<AircraftDO> findByRegistration( String registration );

  /**
   * Busca las aeronaves por el id de la l�nea aerea
   * 
   * @param idCarrier Id de la l�nea a�rea
   * @return una lista de {@link com.axity.example.jpa.model.AircraftDO}
   */
  List<AircraftDO> findByCarrierId( Integer idCarrier );

  /**
   * Busca las aeronaves por el c�digo de la l�nea aerea
   * 
   * @param code C�digo de la l�nea a�rea
   * @return una lista de {@link com.axity.example.jpa.model.AircraftDO}
   */
  List<AircraftDO> findByCarrierCode( String code );

  /**
   * Busca las aeronaves por el id del tipo de aeronave
   * 
   * @param idAircraft Id del tipo de aeronave
   * @return una lista de {@link com.axity.example.jpa.model.AircraftDO}
   */
  List<AircraftDO> findByAircraftId( Integer idAircraft );

  /**
   * Busca las aeronaves por el codigo del tipo de aeronave
   * 
   * @param code C�digo del tipo de aeronave
   * @return una lista de {@link com.axity.example.jpa.model.AircraftDO}
   */
  List<AircraftDO> findByAircraftCode( String code );

  /**
   * Busca las aeronaves que cubre una ruta en un rango de fechas
   * 
   * @param code C�digo de la ruta, i.e. BOGMIA
   * @param start Fecha de inicio
   * @param end Fecha de fin
   * @return una lista de {@link com.axity.example.jpa.model.AircraftDO}
   */
  List<AircraftDO> findByRouteCode( String code, Date start, Date end );

}
