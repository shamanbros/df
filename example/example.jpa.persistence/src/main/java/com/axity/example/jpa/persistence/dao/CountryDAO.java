package com.axity.example.jpa.persistence.dao;

import java.util.List;

import com.axity.example.jpa.model.CountryDO;
import com.axity.example.jpa.persistence.base.GenericDAO;

/**
 * DAO de la tabla C_PAIS
 * 
 * @author gsegura
 */
public interface CountryDAO extends GenericDAO<CountryDO>
{

  /**
   * Busca los pa�ses por el nombre del a�s
   * 
   * @param name Nombre del pa�s
   * @return Una lista de {@link com.axity.example.jpa.model.CountryDO}
   */
  List<CountryDO> findByName( String name );

  /**
   * Busca los paises por el c�digo del pa�s
   * 
   * @param code C�digo del pa�s
   * @return Una lista de {@link com.axity.example.jpa.model.CountryDO}
   */
  List<CountryDO> findByCode( String code );

}
