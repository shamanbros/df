package com.axity.example.jpa.persistence.dao;

import java.util.List;

import com.axity.example.jpa.model.RouteDO;
import com.axity.example.jpa.persistence.base.GenericDAO;
import com.axity.example.jpa.persistence.to.FlightTO;

/**
 * DAO de la tabla K_RUTA
 * 
 * @author gsegura
 */
public interface RouteDAO extends GenericDAO<RouteDO>
{

  List<RouteDO> findRoutesByAircraftRegistration( String registration );

  List<FlightTO> findFlightsByAircraftRegistration( String registration );
}
