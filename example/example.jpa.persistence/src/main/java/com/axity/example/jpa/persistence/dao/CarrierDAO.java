package com.axity.example.jpa.persistence.dao;

import java.util.List;

import com.axity.example.jpa.model.CarrierDO;
import com.axity.example.jpa.persistence.base.GenericDAO;

/**
 * DAO de la tabla C_AEROLINEA
 * 
 * @author gsegura
 */
public interface CarrierDAO extends GenericDAO<CarrierDO>
{

  /**
   * Busca los registros por el c�digo de la aerol�nea
   * 
   * @param code C�digo de la aerol�nea
   * @return Una lista de {@link com.axity.example.jpa.model.CarrierDO}
   */
  List<CarrierDO> findByCode( String code );

  /**
   * Busca los registros por el nombre de la aerol�nea
   * 
   * @param name Nombre de la aerol�nea
   * @return Una lista de {@link com.axity.example.jpa.model.CarrierDO}
   */
  List<CarrierDO> findByName( String name );
}
