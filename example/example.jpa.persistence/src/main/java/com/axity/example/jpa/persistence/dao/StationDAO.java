package com.axity.example.jpa.persistence.dao;

import com.axity.example.jpa.model.StationDO;
import com.axity.example.jpa.persistence.base.GenericDAO;

/**
 * DAO de la tabla C_ESTACION
 * @author gsegura
 *
 */
public interface StationDAO extends GenericDAO<StationDO>
{

}
