package com.axity.example.jpa.persistence.to;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class FlightTO
{

  private int idFlight;
  private int flight;
  private String departure;
  private String arrive;
  private Date scheduledDate;
  private String registration;

  public FlightTO()
  {
  }

  public FlightTO( int idFlight, int flight, String departure, String arrive, Date scheduledDate, String registration )
  {
    this.idFlight = idFlight;
    this.flight = flight;
    this.departure = departure;
    this.arrive = arrive;
    this.scheduledDate = scheduledDate;
    this.registration = registration;
  }

  /**
   * @return the idFlight
   */
  public int getIdFlight()
  {
    return idFlight;
  }

  /**
   * @param idFlight the idFlight to set
   */
  public void setIdFlight( int idFlight )
  {
    this.idFlight = idFlight;
  }

  /**
   * @return the flight
   */
  public int getFlight()
  {
    return flight;
  }

  /**
   * @param flight the flight to set
   */
  public void setFlight( int flight )
  {
    this.flight = flight;
  }

  /**
   * @return the departure
   */
  public String getDeparture()
  {
    return departure;
  }

  /**
   * @param departure the departure to set
   */
  public void setDeparture( String departure )
  {
    this.departure = departure;
  }

  /**
   * @return the arrive
   */
  public String getArrive()
  {
    return arrive;
  }

  /**
   * @param arrive the arrive to set
   */
  public void setArrive( String arrive )
  {
    this.arrive = arrive;
  }

  /**
   * @return the scheduledDate
   */
  public Date getScheduledDate()
  {
    return scheduledDate;
  }

  /**
   * @param scheduledDate the scheduledDate to set
   */
  public void setScheduledDate( Date scheduledDate )
  {
    this.scheduledDate = scheduledDate;
  }

  @Override
  public String toString()
  {
    DateFormat df = new SimpleDateFormat( "yyyy-MM-dd HH:mm" );

    String schedule = "";
    if( this.scheduledDate != null )
    {
      schedule = df.format( scheduledDate );
    }

    NumberFormat nf = new DecimalFormat( "0000" );

    return new ToStringBuilder( this ).append( "idFlight", this.idFlight )
        .append( "flightCode",
          new StringBuilder().append( nf.format( this.flight ) ).append( StringUtils.defaultString( this.departure ) )
              .append( StringUtils.defaultString( this.arrive ) ).toString() )
        .append( "registration", this.registration )
        .append( "scheduledDate", schedule ).toString();

  }
}
