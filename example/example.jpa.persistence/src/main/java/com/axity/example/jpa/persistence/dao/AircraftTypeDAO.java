package com.axity.example.jpa.persistence.dao;

import java.util.List;

import com.axity.example.jpa.model.AircraftTypeDO;
import com.axity.example.jpa.persistence.base.GenericDAO;

/**
 * DAO de la tabla C_TIPO_AERONAVE
 * 
 * @author gsegura
 */
public interface AircraftTypeDAO extends GenericDAO<AircraftTypeDO>
{

  /**
   * Busca los tipos de aeronave por el nombre
   * 
   * @param name Nombre de la aeronave
   * @return Una lista de {@link com.axity.example.jpa.model.AircraftTypeDO}
   */
  List<AircraftTypeDO> findByName( String name );

  /**
   * Busca los tipos de aeronave por el c�digo 
   * 
   * @param code C�digo de la aeronave
   * @return Una lista de {@link com.axity.example.jpa.model.AircraftTypeDO}
   */
  List<AircraftTypeDO> findByCode( String code );

}
