package com.axity.example.jpa.persistence.dao.impl;

import java.util.List;

import javax.persistence.TypedQuery;

import com.axity.example.jpa.model.RouteDO;
import com.axity.example.jpa.persistence.base.GenericBaseDAOImpl;
import com.axity.example.jpa.persistence.dao.RouteDAO;
import com.axity.example.jpa.persistence.to.FlightTO;

public class RouteDAOImpl extends GenericBaseDAOImpl<RouteDO> implements RouteDAO
{

  public RouteDAOImpl()
  {
    super( RouteDO.class );
  }

  @Override
  public List<RouteDO> findRoutesByAircraftRegistration( String registration )
  {
    TypedQuery<RouteDO> query = getEntityManager().createNamedQuery( "RouteDO.findRoutesByAircraftRegistration",
      RouteDO.class );

    query.setParameter( "registration", registration );
    return super.findByNamedQuery( query );
  }

  @Override
  public List<FlightTO> findFlightsByAircraftRegistration( String registration )
  {
    TypedQuery<FlightTO> query = getEntityManager().createNamedQuery( "RouteDO.findFlightsByAircraftRegistration",
      FlightTO.class );
    query.setParameter( "registration", registration );

    return query.getResultList();

  }

}
