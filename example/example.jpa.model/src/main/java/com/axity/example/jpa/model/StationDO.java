package com.axity.example.jpa.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
@Table(name = "C_ESTACION")
public class StationDO extends AbstractCatalogEntity<StationDO>
{
  private static final long serialVersionUID = -7275591535697735121L;

  @Id
  @Column(name = "ID_ESTACION")
  private Integer idStation;

  @JoinColumn(name = "ID_PAIS", referencedColumnName = "ID_PAIS")
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  private CountryDO country;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "departure")
  private List<RouteDO> departureRoutes;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "arrive")
  private List<RouteDO> arriveRoutes;

  /**
   * @return the departureRoutes
   */
  public List<RouteDO> getDepartureRoutes()
  {
    return departureRoutes;
  }

  /**
   * @param departureRoutes the departureRoutes to set
   */
  public void setDepartureRoutes( List<RouteDO> departureRoutes )
  {
    this.departureRoutes = departureRoutes;
  }

  /**
   * @return the arriveRoutes
   */
  public List<RouteDO> getArriveRoutes()
  {
    return arriveRoutes;
  }

  /**
   * @param arriveRoutes the arriveRoutes to set
   */
  public void setArriveRoutes( List<RouteDO> arriveRoutes )
  {
    this.arriveRoutes = arriveRoutes;
  }

  /**
   * @return the idStation
   */
  public Integer getIdStation()
  {
    return idStation;
  }

  /**
   * @param idStation the idStation to set
   */
  public void setIdStation( Integer idStation )
  {
    this.idStation = idStation;
  }

  /**
   * @return the country
   */
  public CountryDO getCountry()
  {
    return country;
  }

  /**
   * @param country the country to set
   */
  public void setCountry( CountryDO country )
  {
    this.country = country;
  }

  @Override
  public boolean equals( Object object )
  {
    boolean isEquals = false;
    if( this == object )
    {
      isEquals = true;
    }
    else if( object != null && object.getClass().equals( this.getClass() ) )
    {
      StationDO that = (StationDO) object;
      isEquals = new EqualsBuilder().append( this.idStation, that.idStation ).isEquals();
    }
    return isEquals;
  }

  @Override
  public int hashCode()
  {
    return new HashCodeBuilder().append( this.idStation ).toHashCode();
  }

  @Override
  public String toString()
  {
    return new ToStringBuilder( this ).append( "idStation", this.idStation ).append( "name", this.name )
        .append( "code", this.code ).toString();
  }

}
