package com.axity.example.jpa.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
@Table(name = "C_TIPO_AERONAVE")
public class AircraftTypeDO extends AbstractCatalogEntity<AircraftTypeDO>
{
  private static final long serialVersionUID = -5185658526287853463L;

  @Id
  @Column(name = "ID_TIPO_AERONAVE")
  private Integer idAircraftType;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "aircraftType")
  private List<AircraftDO> aircrafts;

  /**
   * @return the idAircraftType
   */
  public Integer getIdAircraftType()
  {
    return idAircraftType;
  }

  /**
   * @return the aircrafts
   */
  public List<AircraftDO> getAircrafts()
  {
    return aircrafts;
  }

  /**
   * @param aircrafts the aircrafts to set
   */
  public void setAircrafts( List<AircraftDO> aircrafts )
  {
    this.aircrafts = aircrafts;
  }

  /**
   * @param idAircraftType the idAircraftType to set
   */
  public void setIdAircraftType( Integer idAircraftType )
  {
    this.idAircraftType = idAircraftType;
  }

  @Override
  public boolean equals( Object object )
  {
    boolean isEquals = false;
    if( this == object )
    {
      isEquals = true;
    }
    else if( object != null && object.getClass().equals( this.getClass() ) )
    {
      AircraftTypeDO that = (AircraftTypeDO) object;
      isEquals = new EqualsBuilder().append( this.idAircraftType, that.idAircraftType ).isEquals();
    }
    return isEquals;
  }

  @Override
  public int hashCode()
  {
    return new HashCodeBuilder().append( this.idAircraftType ).toHashCode();
  }

  @Override
  public String toString()
  {
    return new ToStringBuilder( this ).append( "idAircraftType", this.idAircraftType ).append( "name", this.name )
        .append( "code", this.code ).toString();
  }

}
