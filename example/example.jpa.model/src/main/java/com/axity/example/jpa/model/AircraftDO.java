package com.axity.example.jpa.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
@Table(name = "K_AERONAVE")
@NamedQueries({
  @NamedQuery(name="AircraftDO.findByRegistration",
      query = "SELECT o FROM AircraftDO o WHERE o.registration = :registration"),
  @NamedQuery(name="AircraftDO.findByCarrierId", 
      query="SELECT o FROM AircraftDO o WHERE o.carrier.idCarrier = :idCarrier"),
  @NamedQuery(name="AircraftDO.findByAircraftCode",
      query="SELECT o FROM AircraftDO o WHERE o.aircraftType.code = :code"),
  @NamedQuery(name="AircraftDO.findByAircraftId",
      query="SELECT o FROM AircraftDO o WHERE o.aircraftType.idAircraftType = :idAircraft"),
  @NamedQuery(name="AircraftDO.findByCarrierCode",
      query="SELECT o FROM AircraftDO o WHERE o.carrier.code = :code"),
  @NamedQuery(name="AircraftDO.findByRouteCode",
      query="SELECT o FROM AircraftDO o "
          + " INNER JOIN o.flights f"
          + " INNER JOIN f.route r"
          + " WHERE r.code = :code"
          + " AND f.scheduledDepartureUtc BETWEEN :start AND :end")
})
public class AircraftDO extends AbstractEntity<AircraftDO>
{

  /**
   * 
   */
  private static final long serialVersionUID = 6338665657128763522L;

  @Id
  @Column(name = "ID_AERONAVE")
  private Integer idAircraft;

  @Column(name = "DS_MATRICULA")
  private String registration;

  @JoinColumn(name = "ID_AEROLINEA", referencedColumnName = "ID_AEROLINEA")
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  private CarrierDO carrier;

  @JoinColumn(name = "ID_TIPO_AERONAVE", referencedColumnName = "ID_TIPO_AERONAVE")
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  private AircraftTypeDO aircraftType;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "aircraft")
  private List<FlightDO> flights;

  /**
   * @return the idAircraft
   */
  public Integer getIdAircraft()
  {
    return idAircraft;
  }

  /**
   * @param idAircraft the idAircraft to set
   */
  public void setIdAircraft( Integer idAircraft )
  {
    this.idAircraft = idAircraft;
  }

  /**
   * @return the registration
   */
  public String getRegistration()
  {
    return registration;
  }

  /**
   * @param registration the registration to set
   */
  public void setRegistration( String registration )
  {
    this.registration = registration;
  }

  /**
   * @return the carrier
   */
  public CarrierDO getCarrier()
  {
    return carrier;
  }

  /**
   * @param carrier the carrier to set
   */
  public void setCarrier( CarrierDO carrier )
  {
    this.carrier = carrier;
  }

  /**
   * @return the aircraftType
   */
  public AircraftTypeDO getAircraftType()
  {
    return aircraftType;
  }

  /**
   * @param aircraftType the aircraftType to set
   */
  public void setAircraftType( AircraftTypeDO aircraftType )
  {
    this.aircraftType = aircraftType;
  }

  /**
   * @return the flights
   */
  public List<FlightDO> getFlights()
  {
    return flights;
  }

  /**
   * @param flights the flights to set
   */
  public void setFlights( List<FlightDO> flights )
  {
    this.flights = flights;
  }

  @Override
  public boolean equals( Object object )
  {
    boolean isEquals = false;
    if( this == object )
    {
      isEquals = true;
    }
    else if( object != null && object.getClass().equals( this.getClass() ) )
    {
      AircraftDO that = (AircraftDO) object;
      isEquals = new EqualsBuilder().append( this.idAircraft, that.idAircraft ).isEquals();
    }
    return isEquals;
  }

  @Override
  public int hashCode()
  {
    return new HashCodeBuilder().append( this.idAircraft ).toHashCode();
  }

  @Override
  public String toString()
  {
    return new ToStringBuilder( this ).append( "idAircraft", this.idAircraft )
        .append( "registration", this.registration ).toString();
  }

}
