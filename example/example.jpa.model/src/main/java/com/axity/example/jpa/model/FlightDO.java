package com.axity.example.jpa.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
@Table(name = "K_VUELO")
@NamedQueries({
  @NamedQuery(name="FlightDO.findByDates",
      query="SELECT o FROM FlightDO o WHERE o.scheduledDepartureUtc BETWEEN :start AND :end"),
  @NamedQuery(name="FlightDO.findByRouteCode",
      query="SELECT o FROM FlightDO o WHERE o.route.code = :code"
          + " AND o.scheduledDepartureUtc BETWEEN :start AND :end"),
  @NamedQuery(name="FlightDO.findByStationDepartureCode",
      query="SELECT o FROM FlightDO o WHERE o.route.departure.code = :departureStationCode"
          + " AND o.scheduledDepartureUtc BETWEEN :start AND :end"),
  @NamedQuery(name="FlightDO.findByCarrier",
  query="SELECT o FROM FlightDO o WHERE o.aircraft.carrier.code = :carrierCode"
      + " AND o.scheduledDepartureUtc BETWEEN :start AND :end"),
  @NamedQuery(name="FlightDO.findByRegistration",
  query="SELECT o FROM FlightDO o WHERE o.aircraft.registration = :registration"
      + " AND o.scheduledDepartureUtc BETWEEN :start AND :end"),
  @NamedQuery(name="FlightDO.findByFlight",
  query="SELECT o FROM FlightDO o WHERE o.flight = :flight"
      + " AND o.scheduledDepartureUtc BETWEEN :start AND :end"),
  @NamedQuery(name="FlightDO.findByDelayDeparture",
  query="SELECT o FROM FlightDO o WHERE o.delayDeparture > 0"
      + " AND o.scheduledDepartureUtc BETWEEN :start AND :end"),
  @NamedQuery(name="FlightDO.findByDelayArrive",
  query="SELECT o FROM FlightDO o WHERE o.delayArrive > 0"
      + " AND o.scheduledDepartureUtc BETWEEN :start AND :end")
})
public class FlightDO extends AbstractEntity<FlightDO>
{
  private static final long serialVersionUID = -4952088238575227838L;

  @Id
  @Column(name = "ID_VUELO")
  private Integer idFlight;

  @JoinColumn(name = "ID_AERONAVE", referencedColumnName = "ID_AERONAVE")
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  private AircraftDO aircraft;

  @JoinColumn(name = "ID_RUTA", referencedColumnName = "ID_RUTA")
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  private RouteDO route;

  @Column(name = "QT_VUELO")
  private Integer flight;

  @Column(name = "DT_SCH_DEPARTURE_UTC")
  @Temporal(TemporalType.TIMESTAMP)
  private Date scheduledDepartureUtc;

  @Column(name = "DT_ACT_DEPARTURE_UTC")
  @Temporal(TemporalType.TIMESTAMP)
  private Date actualDepartureUtc;

  @Column(name = "DT_SCH_ARRIVE_UTC")
  @Temporal(TemporalType.TIMESTAMP)
  private Date scheduledArriveUtc;

  @Column(name = "DT_ACT_ARRIVE_UTC")
  @Temporal(TemporalType.TIMESTAMP)
  private Date actualArriveUtc;

  @Column(name = "QT_TIME_DELAY_DEP")
  private Integer delayDeparture;

  @Column(name = "QT_TIME_DELAY_ARR")
  private Integer delayArrive;

  /**
   * @return the idFlight
   */
  public Integer getIdFlight()
  {
    return idFlight;
  }

  /**
   * @param idFlight the idFlight to set
   */
  public void setIdFlight( Integer idFlight )
  {
    this.idFlight = idFlight;
  }

  /**
   * @return the aircraft
   */
  public AircraftDO getAircraft()
  {
    return aircraft;
  }

  /**
   * @param aircraft the aircraft to set
   */
  public void setAircraft( AircraftDO aircraft )
  {
    this.aircraft = aircraft;
  }

  /**
   * @return the route
   */
  public RouteDO getRoute()
  {
    return route;
  }

  /**
   * @param route the route to set
   */
  public void setRoute( RouteDO route )
  {
    this.route = route;
  }

  /**
   * @return the flight
   */
  public Integer getFlight()
  {
    return flight;
  }

  /**
   * @param flight the flight to set
   */
  public void setFlight( Integer flight )
  {
    this.flight = flight;
  }

  /**
   * @return the scheduledDepartureUtc
   */
  public Date getScheduledDepartureUtc()
  {
    return scheduledDepartureUtc;
  }

  /**
   * @param scheduledDepartureUtc the scheduledDepartureUtc to set
   */
  public void setScheduledDepartureUtc( Date scheduledDepartureUtc )
  {
    this.scheduledDepartureUtc = scheduledDepartureUtc;
  }

  /**
   * @return the actualDepartureUtc
   */
  public Date getActualDepartureUtc()
  {
    return actualDepartureUtc;
  }

  /**
   * @param actualDepartureUtc the actualDepartureUtc to set
   */
  public void setActualDepartureUtc( Date actualDepartureUtc )
  {
    this.actualDepartureUtc = actualDepartureUtc;
  }

  /**
   * @return the scheduledArriveUtc
   */
  public Date getScheduledArriveUtc()
  {
    return scheduledArriveUtc;
  }

  /**
   * @param scheduledArriveUtc the scheduledArriveUtc to set
   */
  public void setScheduledArriveUtc( Date scheduledArriveUtc )
  {
    this.scheduledArriveUtc = scheduledArriveUtc;
  }

  /**
   * @return the actualArriveUtc
   */
  public Date getActualArriveUtc()
  {
    return actualArriveUtc;
  }

  /**
   * @param actualArriveUtc the actualArriveUtc to set
   */
  public void setActualArriveUtc( Date actualArriveUtc )
  {
    this.actualArriveUtc = actualArriveUtc;
  }

  /**
   * @return the delayDeparture
   */
  public Integer getDelayDeparture()
  {
    return delayDeparture;
  }

  /**
   * @param delayDeparture the delayDeparture to set
   */
  public void setDelayDeparture( Integer delayDeparture )
  {
    this.delayDeparture = delayDeparture;
  }

  /**
   * @return the delayArrive
   */
  public Integer getDelayArrive()
  {
    return delayArrive;
  }

  /**
   * @param delayArrive the delayArrive to set
   */
  public void setDelayArrive( Integer delayArrive )
  {
    this.delayArrive = delayArrive;
  }

  @Override
  public boolean equals( Object object )
  {
    boolean isEquals = false;
    if( this == object )
    {
      isEquals = true;
    }
    else if( object != null && object.getClass().equals( this.getClass() ) )
    {
      FlightDO that = (FlightDO) object;
      isEquals = new EqualsBuilder().append( this.idFlight, that.idFlight ).isEquals();
    }
    return isEquals;
  }

  @Override
  public int hashCode()
  {
    return new HashCodeBuilder().append( this.idFlight ).toHashCode();
  }

  @Override
  public String toString()
  {
    return new ToStringBuilder( this ).append( "idFlight", this.idFlight ).append( "flight", this.flight ).toString();
  }

}
