package com.axity.example.jpa.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
@Table(name = "K_RUTA")
@NamedQueries({ @NamedQuery(name = "RouteDO.findRoutesByAircraftRegistration", 
query = "SELECT DISTINCT o FROM RouteDO o"
    + " INNER JOIN o.flights f"
    + " INNER JOIN f.aircraft a"
    + " WHERE a.registration = :registration")
,
  
@NamedQuery(name="RouteDO.findFlightsByAircraftRegistration",
query = "SELECT new com.axity.example.jpa.persistence.to.FlightTO(f.idFlight, f.flight, dep.code, arr.code, f.scheduledDepartureUtc, a.registration) FROM RouteDO o"
    + " INNER JOIN o.departure dep"
    + " INNER JOIN o.arrive arr"
    + " INNER JOIN o.flights f"
    + " INNER JOIN f.aircraft a"
    + " WHERE a.registration = :registration"
    + " ORDER BY f.scheduledDepartureUtc"
)
})
public class RouteDO extends AbstractEntity<RouteDO>
{
  private static final long serialVersionUID = 5045484972773100303L;

  @Id
  @Column(name = "ID_RUTA")
  private Integer idRoute;

  @JoinColumn(name = "ID_ESTACION_DEP", referencedColumnName = "ID_ESTACION")
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  private StationDO departure;

  @JoinColumn(name = "ID_ESTACION_ARR", referencedColumnName = "ID_ESTACION")
  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  private StationDO arrive;

  @Column(name = "DS_CODE")
  private String code;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "route")
  private List<FlightDO> flights;

  /**
   * @return the idRoute
   */
  public Integer getIdRoute()
  {
    return idRoute;
  }

  /**
   * @param idRoute the idRoute to set
   */
  public void setIdRoute( Integer idRoute )
  {
    this.idRoute = idRoute;
  }

  /**
   * @return the departure
   */
  public StationDO getDeparture()
  {
    return departure;
  }

  /**
   * @param departure the departure to set
   */
  public void setDeparture( StationDO departure )
  {
    this.departure = departure;
  }

  /**
   * @return the arrive
   */
  public StationDO getArrive()
  {
    return arrive;
  }

  /**
   * @param arrive the arrive to set
   */
  public void setArrive( StationDO arrive )
  {
    this.arrive = arrive;
  }

  /**
   * @return the code
   */
  public String getCode()
  {
    return code;
  }

  /**
   * @param code the code to set
   */
  public void setCode( String code )
  {
    this.code = code;
  }

  /**
   * @return the flights
   */
  public List<FlightDO> getFlights()
  {
    return flights;
  }

  /**
   * @param flights the flights to set
   */
  public void setFlights( List<FlightDO> flights )
  {
    this.flights = flights;
  }

  @Override
  public boolean equals( Object object )
  {
    boolean isEquals = false;
    if( this == object )
    {
      isEquals = true;
    }
    else if( object != null && object.getClass().equals( this.getClass() ) )
    {
      RouteDO that = (RouteDO) object;
      isEquals = new EqualsBuilder().append( this.idRoute, that.idRoute ).isEquals();
    }
    return isEquals;
  }

  @Override
  public int hashCode()
  {
    return new HashCodeBuilder().append( this.idRoute ).toHashCode();
  }

  @Override
  public String toString()
  {
    
    DateFormat df = new SimpleDateFormat( "yyyy-MM-dd HH:mm" );
    
    
    
    return new ToStringBuilder( this ).append( "idRoute", this.idRoute ).append( "code", this.code ).toString();
  }

}
